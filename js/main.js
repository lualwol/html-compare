var editor;
var editor2;
var mydom;
var dmp;
var codeView;
var removeHtmlContent;
var removeAttr;
var removeAttrExcept;
var keepComments;



$(document).ready(function() {

    mydom = document.documentElement.innerHTML;

    mydomChild = document.documentElement.childNodes;

    editor = CodeMirror.fromTextArea(document.getElementById("code1"), {
        mode: "text/html",
        styleActiveLine: true,
        lineNumbers: true,
        lineWrapping: true,
        extraKeys: {
            "Ctrl-Space": "autocomplete"
        }
    });

    editor2 = CodeMirror.fromTextArea(document.getElementById("code2"), {
        mode: "text/html",
        styleActiveLine: true,
        lineNumbers: true,
        lineWrapping: true,
        extraKeys: {
            "Ctrl-Space": "autocomplete"
        }

    });

   // var  firstWords = "<p> Upload HTML FILES OR PASTE HTML HERE , IF NOT HTML IT WILL BE CONVERTED TO HTML. </p>"
// Upload HTML FILES OR PASTE HTML HERE , IF NOT HTML IT WILL BE CONVERTED TO HTML.</p>
// <!-- I'm A Comment -->
   // editor2.setValue(firstWords);
   //  editor.setValue(firstWords);



});



function readShowhtml(opt_startByte, opt_stopByte) {
    var file = $('#html1').get(0).files[0];
    var file2 = $('#html2').get(0).files[0];
    // if (file == null && ) {
    //   alert('Please select a file!');
    //   return;
    // }
    var reader1 = new FileReader();
    var reader2 = new FileReader();




    if (file == null || file2 == null) {
        alert("Please Select or Drag files");


    } else {



        // If we use onloadend, we need to check the readyState.
        reader1.onloadend = function(evt) {
            if (evt.target.readyState == FileReader.DONE) { // DONE == 2
                var Dom1Result= evt.target.result;
                // var Dom1String = editor.getValue(); 
                // newhtml = $.parseHTML(Dom1String), 
                //     nodeNames = [];
               

               // var base64 = Dom1Result.substring(Dom1Result.indexOf("base64,") + 7);
              //  var binaryString = atob(base64);



                var parser = new DOMParser();
                var doc = parser.parseFromString(Dom1Result, "text/html");


              //  console.log(doc.body);
               // removeText(doc.body);
                doc1Innerhtml = doc.documentElement.innerHTML;
                // $("#code1").htmlBeautifier(doc1Innerhtml);
                output = html_beautify(doc1Innerhtml);
               // console.log(output);

               // editor.setValue(output);

                //start the next reader when first is complete
                //reader2.readAsBinaryString(file2);
                // reader2.readAsDataURL(file2);
                 reader2.readAsText(file2);

            }
        };

        reader2.onloadend = function(evt) {
            // console.log("dom2 called");
            if (evt.target.readyState == FileReader.DONE) { // DONE == 2
                //  document.getElementById('html2dom').textContent = evt.target.result;
                //  console.log("DONE HTML2");
                var Dom2Result = evt.target.result;

              //  var base64 = Dom2Result.substring(Dom2Result.indexOf("base64,") + 7);
              //  var binaryString = atob(base64);

                var parser = new DOMParser();
                var doc2 = parser.parseFromString(Dom2Result, "text/html");

            //    console.log(doc2.body);
            //    removeText(doc2.body);
                doc2_innerhtml = doc2.documentElement.innerHTML;
                output2 = html_beautify(doc2_innerhtml);

               // editor2.setValue(output2);

                //Compare the new textBoxs now,
               diffUsingJS(0);


            }
        };
       // reader1.readAsBinaryString(file);
       // reader1.readAsDataURL(file);
        reader1.readAsText(file);



    }




}

//Here is where the compare happens using diffjs
function diffUsingJS(viewType) {
    "use strict";

    removeHtmlContent = $("#removeHtmlContent").prop('checked');
    removeAttr = $("#removeAttr").prop('checked');
    removeAttrExcept = $("#removeAttrExcept").prop('checked');
    keepComments   = $("#keepComments").prop('checked');


    var editorValue1 = editor.getValue();
    var editorValue2 = editor2.getValue();


    var parser1 = new DOMParser();
    var dochtml1 = parser1.parseFromString(editorValue1, "text/html");
    console.log(dochtml1.body);
    removeText(dochtml1.documentElement);

    var doc1Innerhtml = dochtml1.documentElement.innerHTML;
    // $("#code1").htmlBeautifier(doc1Innerhtml);
    var output = html_beautify(doc1Innerhtml);
    // console.log(output);

    //editor.setValue(output);


    var parser2 = new DOMParser();
    var dochtml2 = parser2.parseFromString(editorValue2, "text/html");
    console.log(dochtml2.body);
    removeText(dochtml2.documentElement);

    var doc2Innerhtml = dochtml2.documentElement.innerHTML;
    // $("#code1").htmlBeautifier(doc1Innerhtml);
    var output2 = html_beautify(doc2Innerhtml);
    // console.log(output);

    //editor2.setValue(output2);



    // var output1 = html_beautify(editorValue1);
    // var output2 = html_beautify(editorValue2);

    var byId = function(id) {
            return document.getElementById(id);
        },
        base = difflib.stringAsLines(output),
        newtxt = difflib.stringAsLines(output2),
        sm = new difflib.SequenceMatcher(base, newtxt),
        opcodes = sm.get_opcodes(),
        diffoutputdiv = document.getElementById("diffoutput"),
        contextSize = contextSize || null;
    // console.log(sm.ratio());
    // console.log(sm.quick_ratio());
    if (sm.ratio() == 1) {
        // console.log("equal, identical , whatever");
        $("#diffinfo").text("Files identical");
        console.log(output);

    } else {
        console.log("Not identical");
        $("#diffinfo").text("Not identical");

    }


    // contextSize = byId("contextSize").value;

    diffoutputdiv.innerHTML = "";


    diffoutputdiv.appendChild(diffview.buildView({
        baseTextLines: base,
        newTextLines: newtxt,
        opcodes: opcodes,
        baseTextName: "Base Text",
        newTextName: "New Text",
        contextSize: contextSize,
        viewType: viewType
    }));
}




//remove unwanted text nodes from html so only the structure can be compared
function removeText(node) {
    var counter = 0;




 if (node.nodeType === 3) {
        counter++;

        if (removeHtmlContent) {
             node.textContent = '';
        };
       


    } else if (node.nodeType === 1) { // if it is an element node, 
        var children = node.childNodes; // examine the children
        for (var i = 0; i < node.attributes.length; i++) {
            var attrib = node.attributes[i];
           // console.log(attrib);


           if (removeAttrExcept || (removeAttrExcept && removeAttr)) {
            if(attrib.name != "class" && attrib.name != "id"){
            attrib.value = '';   
            }

           };

           if (removeAttr && !removeAttrExcept){
                    attrib.value = '';       

           };
        }
        for (var i = children.length; i--;) {
            counter += removeText(children[i]);
        }
    }else if(node.nodeType === 8){
        //remove code comments 
        if (!keepComments) {

             node.parentNode.removeChild(node);
        };
       
    }
    return counter;








   
}



//changing text on span to show name of file
$("#html1").change(function() {
    var file = $('#html1').get(0).files[0];
   // console.log(file.name + "FILE");
    $("#file1span").text("File Upload:  " + file.name);

    var reader1 = new FileReader();

     reader1.onloadend = function(evt) {
            if (evt.target.readyState == FileReader.DONE) { // DONE == 2
                var Dom1Result= evt.target.result;

               // var base64 = Dom1Result.substring(Dom1Result.indexOf("base64,") + 7);
                //var binaryString = atob(base64);
                var parser = new DOMParser();
                 doc = parser.parseFromString(Dom1Result, "text/html");
                console.log(doc.body);
              //  removeText(doc.body);
                doc1Innerhtml = doc.documentElement.innerHTML;
                // $("#code1").htmlBeautifier(doc1Innerhtml);
                output = html_beautify(doc1Innerhtml);
                // console.log(output);

               editor.setValue(output);
              


            }
        };

  //reader1.readAsBinaryString(file);
   reader1.readAsText(file);

});



$("#html2").change(function() {

    var file2 = $('#html2').get(0).files[0];
  //  console.log(file2.name + "FILE");
    $("#file2span").text("File Upload:  " + file2.name);
    var reader2 = new FileReader();
     reader2.onloadend = function(evt) {
            // console.log("dom2 called");
            if (evt.target.readyState == FileReader.DONE) { // DONE == 2
                //  document.getElementById('html2dom').textContent = evt.target.result;
                //  console.log("DONE HTML2");
                var Dom2Result= evt.target.result;

              //  var base64 = Dom2Result.substring(Dom2Result.indexOf("base64,") + 7);
              //  var binaryString = atob(base64);
              //console.log(binaryString);


                var parser = new DOMParser();
                 doc2 = parser.parseFromString(Dom2Result, "text/html");

               // console.log(doc2.body);
              //  removeText(doc2.body);
               // doc2_innerhtml = doc2.body.innerHTML;
                // documentElement.innerHTML
                 doc2_innerhtml = doc2.documentElement.innerHTML;
                output2 = html_beautify(doc2_innerhtml);

                editor2.setValue(output2);
                



            }
        };


 // reader2.readAsBinaryString(file2);
   //reader2.readAsDataURL(file2);
      reader2.readAsText(file2);


});

